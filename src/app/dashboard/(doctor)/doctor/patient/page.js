const AppointmentTable = () => {
    return (
        <div class="relative overflow-x-auto">
            <h1>Patients</h1><br/>
            <table class="w-full text-sm text-left rtl:text-right text-gray-500">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            Date/Time
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Patient
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Issue
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Tests
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Lab
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Remark
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            16/11/2023
                        </th>
                        <td class="px-6 py-4">
                            Sam
                        </td>
                        <td class="px-6 py-4">
                            Arthritis
                        </td>
                        <td class="px-6 py-4">
                        Patient reports joint pain and stiffness, particularly in the knees and hands. X-rays reveal degeneration of cartilage in affected joints.
                        </td>
                        <td class="px-6 py-4">
                            Lab A
                        </td>
                        <td class="px-6 py-4">
                        Initial consultation. Patient complained of joint pain and stiffness. Ordered X-rays and prescribed pain relievers.
                        </td>
                    </tr>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            18/09/2023
                        </th>
                        <td class="px-6 py-4">
                            Christopher
                        </td>
                        <td class="px-6 py-4">
                            Diabetes
                        </td>
                        <td class="px-6 py-4">
                            Diabetes management review. Adjusted medication and discussed the importance of regular blood glucose monitoring.
                        </td>
                        <td class="px-6 py-4">
                            Lab B
                        </td>
                        <td class="px-6 py-4">
                            Elevated blood sugar levels. Prescribed oral medication, recommended dietary changes, and regular blood glucose monitoring.
                        </td>
                    </tr>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            11/11/2023
                        </th>
                        <td class="px-6 py-4">
                            Sebastian
                        </td>
                        <td class="px-6 py-4">
                            Osteoporosis
                        </td>
                        <td class="px-6 py-4">
                            Decreased bone density observed in a DEXA scan. Prescribed calcium supplements, vitamin D, and advised weight-bearing exercises for bone health.
                        </td>
                        <td class="px-6 py-4">
                            Lab A
                        </td>
                        <td class="px-6 py-4">
                        DEXA scan results showed osteoporosis. Prescribed supplements and recommended weight-bearing exercises.
                        </td>
                    </tr>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            16/11/2023
                        </th>
                        <td class="px-6 py-4">
                            Sam
                        </td>
                        <td class="px-6 py-4">
                            Arthritis
                        </td>
                        <td class="px-6 py-4">
                        Patient reports joint pain and stiffness, particularly in the knees and hands. X-rays reveal degeneration of cartilage in affected joints.
                        </td>
                        <td class="px-6 py-4">
                            Lab A
                        </td>
                        <td class="px-6 py-4">
                        Initial consultation. Patient complained of joint pain and stiffness. Ordered X-rays and prescribed pain relievers.
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    );
}

export default AppointmentTable;