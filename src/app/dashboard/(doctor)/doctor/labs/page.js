const AppointmentTable = () => {
    return (
        <div class="relative overflow-x-auto">
            <h1>Labs</h1><br/>
            <table class="w-full text-sm text-left rtl:text-right text-gray-500">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            Patient
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Test
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Lab
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Date/Time
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            John
                        </th>
                        <td class="px-6 py-4">
                            Insulin
                        </td>
                        <td class="px-6 py-4">
                            Lab A
                        </td>
                        <td class="px-6 py-4">
                            12/11/2023
                        </td>
                    </tr>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            Walker
                        </th>
                        <td class="px-6 py-4">
                            Blood Pressure
                        </td>
                        <td class="px-6 py-4">
                            Lab B
                        </td>
                        <td class="px-6 py-4">
                            27/11/2023
                        </td>
                    </tr>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            Mark
                        </th>
                        <td class="px-6 py-4">
                            CT Scan
                        </td>
                        <td class="px-6 py-4">
                            Lab A
                        </td>
                        <td class="px-6 py-4">
                            12/11/2023
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    );
}

export default AppointmentTable;