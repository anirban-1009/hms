const AppointmentTable = () => {
    return (
        <div class="relative overflow-x-auto">
            <h1>Reports</h1><br/>
            <table class="w-full text-sm text-left rtl:text-right text-gray-500">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            Patient
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Test
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Remark
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            Mark
                        </th>
                        <td class="px-6 py-4">
                            Insulin
                        </td>
                        <td class="px-6 py-4">
                            High Glucose
                        </td>
                    </tr>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            Catherine
                        </th>
                        <td class="px-6 py-4">
                            MRI
                        </td>
                        <td class="px-6 py-4">
                            Concussion
                        </td>
                    </tr>
                    <tr class="bg-white">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            McColonel
                        </th>
                        <td class="px-6 py-4">
                            Blood Pressure
                        </td>
                        <td class="px-6 py-4">
                            High BP level
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    );
}

export default AppointmentTable;