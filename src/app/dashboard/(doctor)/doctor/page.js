"use client";
import FullCalendar from '@fullcalendar/react';
import listPlugin from '@fullcalendar/list';
import { useEffect, useState } from 'react';
import { databases } from '@/app/appwrite';
import { Query } from 'appwrite';
import { BasicModal } from '../../(patient)/modal';

// Define the functional component
export default function CalendarPage() {
  const [events, setEvents] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [user, setUser] = useState(null);

  const toggleModal = () => {
    setShowModal(!showModal)
  }

  const closeModal = () => {
    setShowModal(false);
  };


  const fetchevents = async () => {
    try{
      const response = await databases.listDocuments(
        '657e292cd7e77c6113e3', '657e293c1ae2389c85bb', [
          Query.equal('doctor', 'Dr.SivaRama')
        ]
      )

      const formattedEvents = response.documents.map((doc) => ({
        title: doc.username,
        start: doc.start,
        end: doc.end,
        backgroundColor: "green",
        borderColor: "red",
        history: doc.history,
        issues: doc.issues
        // Add other properties as needed
      }));
      setEvents(formattedEvents);
    } catch (error) {
      console.error('Error fetching data from Appwrite:', error);
    }
  };
  
  const handleEventClick = (arg) => {
    toggleModal();
    events.map((event) => {
      if (event.title === arg.event.title) {
        setUser(event);
      }
    })
  }
  
  useEffect(() => {
    fetchevents();
  }, []);


  return (
    <div className='h-10'>
      {showModal && <BasicModal
        onClose={closeModal}
        title={"Appointment"}
        onclick={null}
      >
          <div className='mt-2 w-full'>
            <div className='flex justify-center'>
              <h2 className='font-sans text-2xl'>{user.title}</h2>
            </div>
            <p className='text-md'>Patient History</p>
            <div className='box-content h-20 p-4 border border-gray-200 rounded-lg'>
              {user.history}
            </div>
            <p className='text-md'>Patient Issues</p>
            <div className='box-content h-20 p-4 border border-gray-200 rounded-lg'>
              {user.issues}
            </div>
          </div>
        </BasicModal>
      }
      <FullCalendar
        height={700}
        plugins={[listPlugin]}
        initialView='listWeek'
        events={events}
        eventInteractive={true}
        eventClick={
          handleEventClick
        }
      />
    </div>
  );
}
