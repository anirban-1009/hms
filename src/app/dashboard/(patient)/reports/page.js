const AppointmentTable = () => {
    return (
        <div class="relative overflow-x-auto">
            <h1>Reports</h1><br/>
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 ">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            Date/Time
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Doctor
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Remark
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            16/11/2023
                        </th>
                        <td class="px-6 py-4">
                            Dr. P Rajest
                        </td>
                        <td class="px-6 py-4">
                            -
                        </td>
                    </tr>
                    <tr class="bg-white border-b">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            17/11/2023
                        </th>
                        <td class="px-6 py-4">
                            Dr.Shivarama
                        </td>
                        <td class="px-6 py-4">
                            Increase in BP
                        </td>
                    </tr>
                    <tr class="bg-white">
                        <th scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            21/11/2023
                        </th>
                        <td class="px-6 py-4">
                            Dr.Satish
                        </td>
                        <td class="px-6 py-4">
                            Normal BP levels
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    );
}

export default AppointmentTable;