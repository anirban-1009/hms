"use client";
import { useEffect, useState } from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import { CalendarWrapper } from './calendar.styled';
import { Button } from '@/app/(style)/components.styled';
import { BasicModal } from './modal';
import { databases, ID } from '@/app/appwrite';
import { useAuth } from "@/app/(auth)/auth";

const CalendarPage = () => {
  const [showModal, setShowModal] = useState(false)
  const [events, setEvents] = useState([]);
  const [DoctorName, setDocName] = useState(null);
  const [start, setStart] = useState("");
  const [end, setEnd] = useState("");
  const [History, setHist] = useState("");
  const [Prescription, setPres] = useState("");
  const { getUser } = useAuth();
  const [user, setUser] = useState(null)

  const data = {
      'start': start,
      'end': end,
      'doctor': DoctorName,
      'issues': Prescription,
      'history': History,
      'username': user?.name
  };

  const pushData = async() => {
      try {
          await databases.createDocument('657e292cd7e77c6113e3', '657e293c1ae2389c85bb', ID.unique(), data);
          closeModal();
      }
      catch (error) {
          console.error("Registration failed:", error);
          // setError("Registration failed. Please try again.");
      }
  };

  useEffect(() => {
      const fetchUserData = async () => {
        const user = await getUser();
        setUser(user);
        console.log(user)
      };
  
      fetchUserData();
  }, [getUser]);

  const fetchevents = async () => {
    try{
      const response = await databases.listDocuments(
        '657e292cd7e77c6113e3', '657e293c1ae2389c85bb'
      )

      const formattedEvents = response.documents.map((doc) => ({
        title: doc.doctor,
        start: doc.start,
        end: doc.end,
        // Add other properties as needed
      }));
      setEvents(formattedEvents);
    } catch (error) {
      console.error('Error fetching data from Appwrite:', error);
    }
  };

  const toggleModal = () => {
    setShowModal(!showModal)
  }

  const closeModal = () => {
    setShowModal(false);
  };

  useEffect(() => {
    fetchevents()
  }, [])

  return (
    <CalendarWrapper>
      <div className='mx-[67rem] mb-3'>
        <Button onClick={toggleModal}>Book Appointment</Button>
      </div>
      {showModal && <BasicModal
        onClose={closeModal}
        title={"Appointment"}
        onclick={pushData}
      >
        <div className="mt-2 w-full">
          <div className="w-full">
              <label for="doctorName">Doctor Name:</label>
          <select
              id="doctorName"
              name="doctorName"
              className="w-full px-4 py-2 border rounded-md"
              onChange={(e) => {
                  if (e.target.value === "none"){
                      setDocName(null);
                  }
                  else{
                      setDocName(e.target.value);
                  }
              }}
          >
            <option value="none">Select Doctor</option>
            <option value="Dr.SivaRama">Dr.SivaRama</option>
            <option value="Dr.Sidharth">Dr.Sidharth</option>
            <option value="Dr.Arvind">Dr.Arvind</option>
          </select>
        </div>
        </div>
        <div className="mt-2">
            <label for="dateTime">Start:</label><br/>
            <div className="w-full px-4 py-2 border rounded-md">
            <input type="datetime-local" id="dateTime" name="dateTime" onChange={(e) => {
                setStart(e.target.value);
            }}/>
            </div>
        </div>
        <div className="mt-2">
            <label for="dateTime">End:</label><br/>
            <div className="w-full px-4 py-2 border rounded-md">
            <input type="datetime-local" id="dateTime" name="dateTime" onChange={(e) => {
                setEnd(e.target.value);
            }}/>
            </div>
        </div>
        <div className="mt-2">
            <label for="history">History: </label>
            <div className="w-full px-4 py-2 rounded-md">
            <textarea id="history" name="history" onChange={(e) => {
                setHist(e.target.value)
            }}/>
            </div>
        </div>
        <div className="mt-2">
          <label for="Prescription">Prescription: </label>
          <div className="w-full px-4 py-2 rounded-md">
            <textarea id="prescription" name="prescription" onChange={(e) => {
              setPres(e.target.value)
            }}/>
          </div>
        </div>
      </BasicModal>}
      <div className='calendar-container'>
        <FullCalendar
          headerToolbar={false}
          height={700}
          plugins={[dayGridPlugin]}
          initialView='dayGridMonth'
          events={events}
        />
      </div>
    </CalendarWrapper>
  );
}

export default CalendarPage;
