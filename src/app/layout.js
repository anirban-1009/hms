import { Inter } from 'next/font/google'
import './globals.css'
import { AuthProvider } from './(auth)/auth'

const inter = Inter({ subsets: ['latin'] })

export default function RootLayout({ children }) {
  return (
    <AuthProvider>
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
    </AuthProvider>
  )
}
