"use client";

import { useState } from "react";
import { FormContainer, StyledInput, Button } from "../(style)/components.styled";
import { useAuth } from "./auth";
import { useRouter } from "next/navigation";
import "../(style)/styles.css";

const LoginPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);

  const { login, getuser } = useAuth();

  const handleLogin = async () => {
    try {
      await login(email, password);
      setError(null); // Clear any previous errors
    } catch (error) {
      setError('Login failed. Please check your credentials.');
    }
  };

  return (
    <div className="container mx-auto mt-8">
      {error && <p className="text-red-500">{error}</p>}
      <FormContainer>
        {error && <p className="text-red-500">{error}</p>}
        <h1>Login</h1>
        <StyledInput
          type="email"
          value={email}
          placeholder="Email"
          onChange={(e) => setEmail(e.target.value)}
        />
        <StyledInput 
          type="password"
          value={password}
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <div className="bottom-text">
          Don't have an account? <a href="/register">Sign up</a> <br/>
          <a href="#">Forgot Password</a>
        </div>
        <Button onClick={handleLogin}>Login</Button>
      </FormContainer>
    </div>
  );
};

export default LoginPage;
