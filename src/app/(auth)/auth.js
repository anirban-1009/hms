"use client";

import { useState, useEffect, createContext, useContext } from 'react';
import { account, client } from '../appwrite';
import { useRouter } from 'next/navigation';

const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const router = useRouter()

  useEffect(() => {
    const checkAuth = async () => {
      try {
        const response = await client.account.get();
        setUser(response);
      } catch (error) {
        setUser(null);
      } finally {
        setIsLoading(false);
      }
    };

    checkAuth();
  }, []);

  const login = async (email, password) => {
    try {
      // await client.account.createSession(email, password);
      // const response = await client.account.get();
      // setUser(response);
      await account.createEmailSession(email, password)
      setUser(await account.get())
      if (email === "admin@mail.com"){
        router.push('/dashboard/doctor');
      } else {
        router.push('/dashboard')
      }
    } catch (error) {
      console.error('Login failed', error);
    }
  };

  const register = async (id, email, password, username) => {
    try {
        await account.create(id, email, password, username);
        router.push('/');
    } catch (error) {
      console.error("Registration failed:", error);
      setError("Registration failed. Please try again.");
    }
};

  const logout = async () => {
    try {
      await account.deleteSession('current');
      setUser(null);
      router.push('/')
    } catch (error) {
      console.error('Logout failed', error);
    }
  };

  const getUser = async () => {
    try {
      const response = await account.get();
      return response;
    } catch (error) {
      console.error('Error fetching user data', error);
      return null;
    }
  };


  return (
    <AuthContext.Provider value={{ user, isLoading, register,  getUser, login, logout, client }}>
      {children}
    </AuthContext.Provider>
  );
};

const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }
  return context;
};



export { AuthProvider, useAuth };

