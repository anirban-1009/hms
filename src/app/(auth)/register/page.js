"use client";
import { useState } from "react";
import { ID } from "../../appwrite";
import { FormContainer } from "@/app/(style)/components.styled";
import "./../../(style)/styles.css"; // Import the Tailwind CSS stylesheet
import { Button, StyledInput } from "@/app/(style)/components.styled";
import { useRouter } from "next/navigation";
import { useAuth } from "../auth";

const Register = () => {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [username, setName] = useState("");
    const [age, setAge] = useState('');
    const [gender, setGender] = useState("");
    const [error, setError] = useState(null);
    const [phone, setNum] = useState('');

    const {register} = useAuth();

    const handleReg = async() => {
        try {
            await register(ID.unique(), email, password, username);
            setError(null);
        } catch(error) {
            setError('Registration Failed.' + error);
        }
    }

    return (
        <FormContainer>
            {error && <p className="text-red-500">{error}</p>}
            <h1>Register</h1>
            <StyledInput 
                type="email"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
            />
            <StyledInput 
                type="text"
                placeholder="Username"
                value={username}
                onChange={(e) => setName(e.target.value)}
            />
            <StyledInput 
                type="number"
                placeholder="Age"
                value={age}
                onChange={(e) => setAge(e.target.value)}
            />
            <StyledInput 
                type="text"
                placeholder="Gender"
                value={gender}
                onChange={(e) => setGender(e.target.value)}
            />
            <StyledInput 
                type="number"
                placeholder="Number"
                value={phone}
                onChange={(e) => setNum(e.target.value)}
            />
            <StyledInput 
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
            />
             <div class="bottom-text">
                  Don't have account? <a href="/">Login</a> <br/>
                  
                </div>
            <Button
                onClick={handleReg}
            >
                Register
            </Button>
        </FormContainer>
    );

};

export default Register;