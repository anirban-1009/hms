import { Client, Account, Databases } from 'appwrite';

export const client = new Client();

export const Server = {
    endpoint: process.env.ENDPOINT,
    project: process.env.PROJECT,
};


client
    .setEndpoint(Server.endpoint)
    .setProject(Server.project); // Replace with your project ID

export const account = new Account(client);
export const databases = new Databases(client);
export { ID } from 'appwrite';
