"use client";

import Link from 'next/link';
import NavLinks from './nav-links';
import { PowerIcon, ChatBubbleLeftRightIcon } from '@heroicons/react/24/outline';
import { useRouter } from 'next/navigation';
import { useAuth } from '../(auth)/auth';


export default function SideNav() {

  const { logout } = useAuth();
  const router = useRouter();

  const handlelogout = async() => {
    try {
      await logout();
      router.push('/');
      setError(null); // Clear any previous errors
    } catch (error) {
      setError('Login failed. Please check your credentials.');
    }
  }
  
  return (
    <div className="flex h-full flex-col px-3 py-4 md:px-2">
      <div
        className="mb-2 flex h-20 items-center rounded-md bg-blue-600 p-4 md:h-40"
        href="#"
      >
        <div >
        <img src='https://gitlab.com/anirban-1009/hms/-/raw/main/src/app/components/logo.png?ref_type=heads' alt='logo' />
        </div>
      </div>
      <div className="flex grow flex-row justify-between space-x-2 md:flex-col md:space-x-0 md:space-y-2">
        <NavLinks />
        <div className="hidden h-auto w-full grow rounded-md bg-gray-50 md:block"></div>
          <button className="flex h-[48px] w-full grow items-center justify-center gap-2 rounded-md bg-gray-50 p-3 text-sm font-medium hover:bg-sky-100 hover:text-blue-600 md:flex-none md:justify-start md:p-2 md:px-3" href="#">
            <ChatBubbleLeftRightIcon className="w-6" />
            <div className="hidden md:block">Assistant</div>
          </button>
          <button className="flex h-[48px] w-full grow items-center justify-center gap-2 rounded-md bg-gray-50 p-3 text-sm font-medium hover:bg-sky-100 hover:text-blue-600 md:flex-none md:justify-start md:p-2 md:px-3" onClick={logout}>
            <PowerIcon className="w-6" />
            <div className="hidden md:block">Sign Out</div>
          </button>
      </div>
    </div>
  );
}
