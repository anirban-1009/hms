"use client";

import {
  ClockIcon,
  HomeIcon,
  DocumentDuplicateIcon,
  CurrencyDollarIcon,
  UsersIcon,
  BeakerIcon
} from '@heroicons/react/24/outline';

import Link from 'next/link';
import { usePathname } from 'next/navigation';
import clsx from 'clsx';
const getLinks = (pathname) => {
  if (pathname.includes('doctor')) {
    return [
      { name: 'Home', href: '/dashboard/doctor', icon: HomeIcon },
      { name: 'Patients', href: '/dashboard/doctor/patient', icon: UsersIcon },
      { name: 'Labs', href: '/dashboard/doctor/labs', icon: BeakerIcon },
      { name: 'Reports', href: '/dashboard/doctor/reports', icon: DocumentDuplicateIcon },
    ];
  } else {
    return [
      { name: 'Home', href: '/dashboard', icon: HomeIcon },
      {
        name: 'Reports',
        href: '/dashboard/reports',
        icon: DocumentDuplicateIcon,
      },
      {
        name: 'History',
        href: '/dashboard/history',
        icon: ClockIcon,
      },
      {
        name: 'Insurance',
        href: '#',
        icon: CurrencyDollarIcon,
      },
    ];
  }
}
const links = [
  { name: 'Home', href: '/dashboard', icon: HomeIcon },
  {
    name: 'Reports',
    href: '#',
    icon: DocumentDuplicateIcon,
  },
  {
    name: 'History',
    href: '#',
    icon: ClockIcon,
  },
  {
    name: 'Insurance',
    href: '#',
    icon: CurrencyDollarIcon,
  },
];

export default function NavLinks() {
  const pathname = usePathname();
  const links = getLinks(pathname);

  return (
    <>
      {links.map((link) => {
        const LinkIcon = link.icon;
        return (
          <Link
            key={link.name}
            href={link.href}
            className={clsx(
              "flex h-[48px] grow items-center justify-center gap-2 rounded-md bg-gray-50 p-3 text-sm font-medium hover:bg-sky-100 hover:text-blue-600 md:flex-none md:justify-start md:p-2 md:px-3",
              {
                'bg-sky-100 text-blue-600': pathname === link.href,
              }
            )}
          >
            <LinkIcon className="w-6" />
            <p className="hidden md:block">{link.name}</p>
          </Link>
        );
      })}
    </>
  );
}
