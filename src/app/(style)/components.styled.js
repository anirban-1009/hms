import styled from "styled-components";

export const Button = styled.div`
    border: none;
    color: #fff;
    font-weight: bold;
    padding: 8px 16px;
    border-radius: 4px;
    cursor: pointer;
    text-align: center;
    width: 180px;
    margin: auto;
    display: block;
    background: linear-gradient(120deg, #3498db, #8e44ad);
    background-size: 200%;
    transition: 0.5s;

    &:hover {
        background-color: ${(props) => props.hoverColor || '#2980b9'};
        background-position: right;
        }
    }
`;

export const StyledInput = styled.input`
  padding: 8px;
  border: 3px solid #ccc;
  border-radius: 7px;
  color: ${(props) => props.textColor || '#333'}; /* Default text color */
  font-size: 21px;

  &::placeholder {
    color: ${(props) => props.placeholderColor || '#999'}; /* Default placeholder color */
  }

  &:focus {
    outline: none;
    border-color: ${(props) => props.focusColor || '#3498db'}; /* Default focus color */
  }
`;

export const FormContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    height: auto;
    width: 400px;
    margin: auto;
    margin-top: 130px;
    padding: 10px;
    padding-bottom: 80px;
    border-radius: 10px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    background-color: #ffffff;
    gap: 30px;

    h1 {
        margin-top: 28px;
        font-size: 32px;
        color: #333333;
        margin-bottom: 10px;
        font-weight: bold;
    }
`;

