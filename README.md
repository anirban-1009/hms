# Hospital Management System (HMS)

![Project Logo/Image](https://gitlab.com/anirban-1009/hms/-/raw/main/src/app/components/logo.png?ref_type=heads)

## Overview

The Hospital Management System (HMS) is a web-based application designed to streamline appointment scheduling and management for healthcare facilities. It allows patients to schedule appointments and provides doctors with a convenient interface to view their schedules.

### Features

- User authentication for patients and doctors.
- Appointment scheduling with a user-friendly calendar interface.
- Separate views for patients and doctors.
- Integration with Appwrite for backend services.
- Responsive UI design using Next.js, Tailwind CSS, and FullCalendar.js.

## Table of Contents

1. [Getting Started](#getting-started)
   - [Prerequisites](#prerequisites)
   - [Installation](#installation)
2. [Usage](#usage)
   - [Login](#login)
   - [Appointment Booking](#appointment-booking)
   - [Doctor's Schedule](#doctors-schedule)
3. [Tech Stack](#tech-stack)
4. [Contributing](#contributing)
5. [License](#license)

## Getting Started

### Prerequisites

- Node.js and npm installed
- Git
- Appwrite API key (for backend services)

### Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/your-username/hospital-management-system.git
cd hospital-management-system
```

2. Install dependencies:

```bash
npm install
```

3. Set up environment variables:

   - Create a `.env.local` file in the project root and add necessary variables, such as Appwrite API key.

4. Start the development server:

```bash
npm run dev
```

Visit `http://localhost:3000` in your browser.

## Usage

### Login

- Access the login page and enter your credentials.

### Appointment Booking

- Navigate to the appointment page.
- Select a suitable date and time.
- Choose your preferred doctor.
- Confirm and schedule the appointment.

### Doctor's Schedule

- Log in as a doctor to view the weekly schedule.
- Manage and reschedule appointments as needed.

## Tech Stack

- [Next.js](https://nextjs.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [FullCalendar.js](https://fullcalendar.io/)
- [Appwrite](https://appwrite.io/)

## WireFrames

[HMS View](https://www.figma.com/file/ag0ZZaTQGYDCmDN7sStwEA/hack-the-verse?type=design&fuid=1208273488580121361)

## Contributing

Contributions are welcome! See [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## License

This project is licensed under the [GNU Affero General Public License v3.0](LICENSE).
