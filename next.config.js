/** @type {import('next').NextConfig} */
const nextConfig = {
    env: {
        ENDPOINT: process.env.ENDPOINT,
        PROJECT: process.env.PROJECT,
    },
}

module.exports = nextConfig
